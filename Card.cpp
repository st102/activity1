//
//  Card.cpp
//  ProgrammingAssignment#1
//
//  Created by Sagar Timsina on 9/16/18.
//  Copyright © 2018 Sagar Timsina. All rights reserved.
//

#include "Card.hpp"
#include <iostream>
#include <ctime>
#include <cstdlib>

using namespace std;

Card::Card()
{
}

void Card::ShuffelCard()
{
    srand (time(0));
    int randomNumber = rand() % 4;
    
    if (randomNumber == Hearts)
    {
        suit = Hearts;
    }
    else if (randomNumber == Spades)
    {
        suit = Spades;
    }
    else if (randomNumber == Clubs)
    {
        suit = Clubs;
    }
    else if (randomNumber == Diamonds)
    {
        suit = Diamonds;
    }
    
    randomNumber = rand() % 13 + 1;
    
    if(randomNumber == Ace)
    {
        rank = Ace;
    }
    else if (randomNumber == Two)
    {
        rank = Two;
    }
    else if (randomNumber == Three)
    {
        rank = Three;
    }
    else if (randomNumber == Four)
    {
        rank = Four;
    }
    else if (randomNumber == Five)
    {
        rank = Five;
    }
    else if (randomNumber == Six)
    {
        rank = Six;
    }
    else if (randomNumber == Seven)
    {
        rank = Seven;
    }
    else if (randomNumber == Eight)
    {
        rank = Eight;
    }
    else if (randomNumber == Nine)
    {
        rank = Nine;
    }
    else if (randomNumber == Ten)
    {
        rank = Ten;
    }
    else if (randomNumber == Jack)
    {
        rank = Jack;
    }
    else if (randomNumber ==Queen)
    {
        rank = Queen;
    }
    else if (randomNumber == King)
    {
        rank = King;
    }
}
