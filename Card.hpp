//
//  Card.hpp
//  ProgrammingAssignment#1
//
//  Created by Sagar Timsina on 9/16/18.
//  Copyright © 2018 Sagar Timsina. All rights reserved.
//

#ifndef Card_hpp
#define Card_hpp
#include <stdio.h>

enum Suit
{
    Spades,
    Clubs,
    Diamonds,
    Hearts
};

enum Rank
{
    Ace = 1,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    Ten,
    Jack,
    Queen,
    King
};

class Card
{
    public:
        Suit suit;
        Rank rank;
    
    public:
        Card();
  
        void ShuffelCard();
    
        Suit getRandomSuit()
        {
            return suit;
        }
        Rank getRandomRank()
        {
            return rank;
        }
};

#endif /* Card_hpp */
