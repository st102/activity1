//
//  Card.h
//  ProgrammingAssignment#1
//
//  Created by Sagar Timsina on 9/16/18.
//  Copyright © 2018 Sagar Timsina. All rights reserved.
//

#ifndef Card_h
#define Card_h
#include <ctime>
#include <cstdlib>

using namespace std;
enum Suit
{
    Spades,
    Clubs,
    Diamonds,
    Hearts
};

enum Rank
{
    Ace,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    Ten,
    Jack,
    Queen,
    King
};

class Card
{
    Suit suit;
    Rank rank;
    
};

#endif /* Card_h */
