//
//  main.cpp
//  ProgrammingAssignment#1
//
//  Created by Sagar Timsina on 9/16/18.
//  Copyright © 2018 Sagar Timsina. All rights reserved.
//

#include <iostream>
#include <vector>
#include "Card.hpp"

using namespace std;

int main() {
    
    Card cardObject;
    
    cardObject.ShuffelCard();
    
    cout <<"Random Suit: ";
    cout << cardObject.getRandomSuit() << "\n";
    
    cout <<"Random Rank: ";
    cout << cardObject.getRandomRank() << "\n";
    
    return 0;
}
